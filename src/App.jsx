import { Switch, Route } from "react-router-dom";
import { useState } from "react";

import NavBar from "./components/Navbar";
import Home from "./pages/Home";
import Completed from "./pages/Completed";

import "./App.scss";

function App() {
  const [todos, setTodos] = useState([]);
  const [completed, setCompleted] = useState([]);

  function completeTask(id) {
    // 1. Encontrar la tarea de todos que quiero completar y sacarla del array (find)
    // Find encuentra el primer elemento de un array que cumpla una condicion

    const taskToComplete = todos.find((todo) => {
      return todo.id === id;
    });

    // 2. Actualizar todos para que NO tenga esa tarea dentro (filter)
    // Filter crea un nueva array con todos los elementos que cumplen una condicion

    const newTasks = todos.filter((todo) => {
      return todo.id !== id;
    });

    setTodos(newTasks)

    // 3. Introducir el todo que hemos sacado de todos en completed
    setCompleted([...completed, taskToComplete]);
  }

  return (
    <div className="App">
      <NavBar />

      <Switch>
        <Route path="/" exact>
          <Home todos={todos} setTodos={setTodos} completeTask={completeTask}/>
        </Route>

        <Route path="/completed" exact>
          <Completed completed={completed} setCompleted={setCompleted} />
        </Route>
      </Switch>
    </div>
  );
}

export default App;

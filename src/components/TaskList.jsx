import "../styles/TaskList.scss";

function TaskList({ todos, completeTask }) {
  return (
    <ul className="TaskList">
      {todos.map((todos) => {
        return (
          <li className="TaskList__item" key={todos.id}>
            <h4 className="TaskList__item-title">
              {todos.category} - {todos.task}
            </h4>
            <p className="TaskList__item-description">{todos.description}</p>

            {completeTask ? (
              <button className="TaskList__button" type="button" onClick={() => completeTask(todos.id)}>
                Tarea Completada
              </button>
            ) : null}
          </li>
        );
      })}
    </ul>
  );
}

export default TaskList;

import { useState } from "react";

import "../styles/AddTask.scss";

import { v4 as uuid } from "uuid";

function AddTask({ todos, setIsActive, addTask }) {
  const [todosForm, setTodosForm] = useState({
    task: "",
    category: "",
    description: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    // 1. Ensamblo un nuevo task
    const newTask = {
      ...todosForm,
      id: uuid(),
    };
    // 2. Envío task a Home
    addTask(newTask);

    // 3. Limpio mi formulario
    setTodosForm({
      task: "",
      category: "",
      description: "",
    });

    setIsActive(false);
  };

  const handleChangeInput = (event) => {
    const { name, value } = event.target;

    setTodosForm({
      ...todosForm,
      [name]: value,
    });
  };

  console.log({ todosForm, todos });

  return (
    <form onSubmit={handleSubmit} className="AddTask">
      <fieldset className="AddTask__fieldset">
        <h3 className="AddTask__fieldset__title">Tarea</h3>
        <input
          className="AddTask__fieldset__input"
          required
          id="task"
          name="task"
          type="text"
          value={todosForm.task}
          onChange={handleChangeInput}
        />
      </fieldset>

      <fieldset className="AddTask__fieldset">
        <h3 className="AddTask__fieldset__title">Categoría</h3>
        <input
          className="AddTask__fieldset__input"
          id="category"
          name="category"
          type="text"
          value={todosForm.category}
          onChange={handleChangeInput}
        />
      </fieldset>

      <fieldset className="AddTask__fieldset">
        <h3 className="AddTask__fieldset__title">Descripción</h3>
        <textarea
          className="AddTask__fieldset__textarea"
          id="description"
          name="description"
          value={todosForm.description}
          onChange={handleChangeInput}
        />
      </fieldset>

      <button className="AddTask__button" type="submit">
        Añadir Tarea
      </button>
    </form>
  );
}

export default AddTask;

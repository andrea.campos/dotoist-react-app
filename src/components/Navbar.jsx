import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";

import "../styles/Navbar.scss";


function NavBar() {
  return (
    <header className="NavBar">
      <Link to="/">
        <FontAwesomeIcon className="NavBar__icon-home" icon={faHome} />
      </Link>

      <Link to="/completed" className="NavBar__task-completed">
        Tareas Completadas
      </Link>
    </header>
  );
}

export default NavBar;

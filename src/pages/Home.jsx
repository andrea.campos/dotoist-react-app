import { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { faMinusCircle } from "@fortawesome/free-solid-svg-icons";

import "../styles/Home.scss";
import AddTask from "../components/AddTask";
import TaskList from "../components/TaskList";

function Home({ todos, setTodos, completeTask }) {
  const [isActive, setIsActive] = useState(false);

  function addTask(newTask) {
    setTodos([...todos, newTask])
  }

  return (
    <main className="Home">
      {todos.length > 0 ? null : (
        <p className="Home__disclamer">No tienes tareas pendientes 🎉</p>
      )}
      <div className="Home__add-task">
        {isActive ? (<FontAwesomeIcon className="Home__add-task__icon" icon={faMinusCircle}/>) : (<FontAwesomeIcon className="Home__add-task__icon" icon={faPlusCircle}/>)}
        <span
          className="Home__add-task__text"
          onClick={() => {
            setIsActive(!isActive);
          }}
        >
          {isActive ? 'Cancelar ' : 'Añadir Tarea'}
        </span>
      </div>

      {isActive ? (
        <AddTask todos={todos} setTodos={setTodos} setIsActive={setIsActive} addTask={addTask} />
      ) : null}


        <TaskList todos={todos} completeTask={completeTask} />
    </main>
  );
}

export default Home;

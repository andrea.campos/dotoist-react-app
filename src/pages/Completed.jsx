import "../styles/Completed.scss";

import TaskList from "../components/TaskList";

function Completed({ completed, setCompleted }) {
  return (
    <section className="Completed">
      <TaskList todos={completed} />
    </section>
  );
}

export default Completed;
